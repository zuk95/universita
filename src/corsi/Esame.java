package corsi;

import eccezioni.VotoNonValidoException;

public class Esame implements Comparable<Esame>{
    private int voto;
    private boolean sostenuto;

    public Esame() {
    }

    public void setVoto(int voto) {
        if(voto>=18&&voto<=30) {
            this.voto = voto;
            this.sostenuto=true;
        }else{
            throw new VotoNonValidoException();
        }
    }

    public boolean isSostenuto() {
        return sostenuto;
    }

    public int getVoto() {
        return voto;
    }

    private String stampaSostenuto(){
        if(voto!=0){
            return Integer.toString(voto);
        }
        return "NON SOSTENUTO";
    }

    public void setSostenuto(boolean sostenuto) {
        this.sostenuto = sostenuto;
    }

    @Override
    public int compareTo(Esame o) {
        int diff= this.voto - o.getVoto();
        return diff;
    }

    @Override
    public String toString() {
        return "Voto= "+stampaSostenuto();
    }
}
