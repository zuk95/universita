package corsi;

public class Corso {
    private String nome,codice;
    private int cfu;

    public Corso(String nome, String codice, int cfu) {
        this.nome = nome;
        this.codice = codice;
        this.cfu = cfu;
    }

    public int getCfu() {
        return cfu;
    }

    @Override
    public String toString() {
        return "Corso di "+nome+" | "+"CFU: "+cfu+"\n";
    }
}
