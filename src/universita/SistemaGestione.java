package universita;

import corsi.Corso;
import studenti.Studente;

import java.util.HashSet;

public class SistemaGestione {
    private HashSet<Studente> studenti;

    public SistemaGestione() {
        this.studenti=new HashSet<>();
    }

    public boolean addStudente(Studente s){
        return studenti.add(s);
    }

    public void elencoStudentiCorso(Corso c){
        for (Studente s:studenti
             ) {
            System.out.println(s.stampaNomeCognome(c));
        }
    }

    public void visualizzaMedia(Corso c){
        for (Studente s:studenti
             ) {
            if(s.getPianoDiStudio().containsKey(c)&&s.getPianoDiStudio().get(c).isSostenuto()) {
                System.out.print(s.stampaNomeCognome());
                System.out.println("Media Voti= " + s.mediaVoti(c)+"\n");
            }
        }
    }

    public void visualizzaPonderata(){
        for (Studente s:studenti
             ) {
            System.out.print(s.stampaNomeCognome());
            System.out.println(" Media Voti Ponderata= " + s.mediaPonderata()+"\n");
        }
    }


    @Override
    public String toString() {
        return "Lista studenti\n" +
                studenti;
    }
}
