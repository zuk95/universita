package eccezioni;

public class VotoNonValidoException extends RuntimeException {
    public VotoNonValidoException() {
        super("VOTO NON VALIDO,ESAME NON SUPERATO");
    }
}
