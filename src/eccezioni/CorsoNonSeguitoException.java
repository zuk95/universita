package eccezioni;

public class CorsoNonSeguitoException extends RuntimeException {
    public CorsoNonSeguitoException() {
        super("CORSO NON SEGUITO QUINDI IMPOSSIBILE SOSTENERE L'ESAME");
    }
}
