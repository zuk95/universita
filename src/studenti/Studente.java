package studenti;

import corsi.Corso;
import corsi.Esame;
import eccezioni.CorsoNonSeguitoException;
import eccezioni.VotoNonValidoException;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class Studente {
    private String nome,cognome;
    private int matricola;
    protected Map<Corso, Esame> pianoDiStudio;

    public Studente(String nome, String cognome, int matricola) {
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
        this.pianoDiStudio=new LinkedHashMap<>();
    }

    public Map<Corso, Esame> getPianoDiStudio() {
        return pianoDiStudio;
    }

    public void addCorso(Corso c){
        pianoDiStudio.putIfAbsent(c,new Esame());
    }

    public void sostieniEsame(Corso c,int voto){
        try{
            if(!pianoDiStudio.containsKey(c)){
                throw new CorsoNonSeguitoException();
            }
        pianoDiStudio.get(c).setVoto(voto);
        }catch (VotoNonValidoException e){
            System.err.println(e.getLocalizedMessage());
        }catch (CorsoNonSeguitoException t){
            System.err.println(t.getLocalizedMessage());
        }
    }

    public double mediaVoti(Corso c){
        double media=0;
        int divisore=0;
        if(pianoDiStudio.containsKey(c)&&pianoDiStudio.get(c).isSostenuto()) {
            for (Esame e : pianoDiStudio.values()
            ) {
                if (e.isSostenuto()) {
                    media += e.getVoto();
                    divisore++;
                }
            }
        }
        return media/divisore;
    }

    @Override
    public String toString() {
        return nome +" "+cognome+"\n" +
                "------------------\n" +
                pianoDiStudio+
                "\n------------------\n\n";
    }


    public String stampaNomeCognome(Corso c){
        if(pianoDiStudio.containsKey(c)){
            return nome+ " "+cognome+ " "+matricola+"\n";
        }
        return "";
    }

    public String stampaNomeCognome(){
            return nome+ " "+cognome+ " "+matricola+"\n";
    }


    public abstract double mediaPonderata();

}
