package studenti;

import corsi.Corso;

public class Magistrale extends Studente {

    public Magistrale(String nome, String cognome, int matricola) {
        super(nome, cognome, matricola);
    }

    @Override
    public double mediaPonderata() {
        double media=0;
        int divisore=0;
        for (Corso c:pianoDiStudio.keySet()
             ) {
            if(pianoDiStudio.get(c).isSostenuto()){
                media+=pianoDiStudio.get(c).getVoto()*c.getCfu();
                divisore+=c.getCfu();
            }
        }
        return media/divisore;
    }
}
