package studenti;

import corsi.Corso;
import corsi.Esame;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Triennale extends Studente {
    public Triennale(String nome, String cognome, int matricola) {
        super(nome, cognome, matricola);
    }


    @Override
    public double mediaPonderata() {
        pianoDiStudio.entrySet().stream().sorted(Map.Entry.comparingByValue());
        Map<Corso,Esame> piano = pianoDiStudio.entrySet().stream().
                sorted(Map.Entry.comparingByValue()).collect(Collectors.toMap(Map.Entry::getKey,
                Map.Entry::getValue,(e1,e2)->e1, LinkedHashMap::new));

        int i=0;
        for (Corso c:piano.keySet()
             ) {
            if(piano.get(c).isSostenuto()){
                piano.get(c).setSostenuto(false);
                if(++i >=2){
                    break;
                }
            }
        }
        double media=0;
        int divisore=0;
        for (Corso c:piano.keySet()
        ) {
            if(piano.get(c).isSostenuto()){
                media+=piano.get(c).getVoto()*c.getCfu();
                divisore+=c.getCfu();
            }
        }
        return media/divisore;
    }


}
