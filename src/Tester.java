import corsi.Corso;
import studenti.Magistrale;
import studenti.Studente;
import studenti.Triennale;
import universita.SistemaGestione;

public class Tester {
    public static void main(String[] args) {
        Corso analisi = new Corso("Analisi 1","AN1",9);
        Corso controlli = new Corso("Controlli Automatici","CAL",12);
        Corso comunicazioni = new Corso("Comunicazioni Elettriche","CEP",9);
        Corso fisica = new Corso("Fisica 1","FSC",9);
        Corso elettronica = new Corso("Elettronica 1","ELE",9);
        Corso basi = new Corso("Basi di Dati","BDD",6);

        Studente io = new Triennale("Alessandro","Zuccolo",438785);
        Studente aldo = new Triennale("Aldo","Baglio",465764);
        Studente giovanni = new Magistrale("Giovanni","Storti",457675);
        Studente giacomo = new Magistrale("Giacomo","Poretti",463768);
        Studente paolo = new Triennale("Paolo","Maldini",453456);

        io.addCorso(analisi);
        io.addCorso(controlli);
        io.addCorso(elettronica);
        io.sostieniEsame(analisi,28);
        io.sostieniEsame(controlli,25);
        io.sostieniEsame(comunicazioni,24);


        aldo.addCorso(analisi);
        aldo.addCorso(comunicazioni);
        aldo.addCorso(elettronica);
        aldo.sostieniEsame(comunicazioni,27);
        aldo.sostieniEsame(elettronica,20);
        aldo.sostieniEsame(analisi,15);


        giovanni.addCorso(controlli);
        giovanni.addCorso(analisi);
        giovanni.addCorso(fisica);
        giovanni.sostieniEsame(controlli,20);
        giovanni.sostieniEsame(fisica,29);

        giacomo.addCorso(analisi);
        giacomo.addCorso(fisica);
        giacomo.addCorso(basi);
        giacomo.sostieniEsame(fisica,25);

        paolo.addCorso(basi);
        paolo.addCorso(fisica);
        paolo.addCorso(comunicazioni);
        paolo.sostieniEsame(basi,20);
        paolo.sostieniEsame(fisica,18);
        paolo.sostieniEsame(comunicazioni,18);

        //System.out.println(io);
        //System.out.println(aldo);

        SistemaGestione universita = new SistemaGestione();
        universita.addStudente(io);
        universita.addStudente(aldo);
        universita.addStudente(giovanni);
        universita.addStudente(giacomo);
        universita.addStudente(paolo);

        //universita.elencoStudentiCorso(controlli);
        //universita.visualizzaMedia(analisi);
        //universita.visualizzaPonderata();


    }
}
